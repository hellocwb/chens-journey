# :orange: 一个橙汁的后端之旅

# :sparkling_heart:说明 #
> 记录自己的java后端学习之路

## :page_facing_up:Java基础 ##

|        :book:Java集合        |           :memo:Java多线程            |        :ski:JVM         | :guitar:JAVA高级语法                           |
| :--------------------------: | :-----------------------------------: | :---------------------: | ---------------------------------------------- |
| [Java集合](src/java/java.md) | [Java多线程](src/java/multiThread.md) | [jvm](src/javabasic.md) | [泛型反射等](src/java/java-advanced-syntax.md) |

## :page_facing_up:JavaWeb ##

| :ledger:Servlet+Tomcat | :microscope:session/cookie | :ring:JDBC&连接池 | :rabbit2:过滤器与监视器 |
| ---------------------- | -------------------------- | ----------------- | ----------------------- |
| [tomcat+servlet]()     | [session&cookie](src/)     | [jdbc&连接池]()   | [过滤器&监视器]()       |

## :page_facing_up:SSM

| :lantern:**SpringMVC** | :taco:Spring | :hamburger:Mybatis | :jack_o_lantern:SSM整合 |
| ---------------------- | ------------ | ------------------ | ----------------------- |
| [SpringMVC]()          | [Spring]()   | [Mybatis]()        | [SSM整合]()             |

## :page_facing_up:Springboot

| :ghost:SpringBoot |
| ----------------- |
| [springboot]()    |

## :page_facing_up:SpringCloud

| :cloud:SpringCloud |
| ------------------ |
| [springboot]()     |

## :page_facing_up:MiddleWare

| :hamster: **网关层中间件** | :goat: **服务层中间件** | :yum: **数据层** |
| -------------------------- | ----------------------- | ---------------- |
| [网关层中间件]()           | [服务层中间件]()        | [数据层]()       |

## :page_with_curl:算法与数据结构

|      |
| ---- |
|      |

## :page_facing_up:加密与安全

| :mag:对称加密算法                                            | :game_die:哈希算法                                           |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [AES](https://blog.csdn.net/chen2016/article/details/113358155) | [MD5](https://www.liaoxuefeng.com/wiki/1252599548343744/1304227729113121) |
|                                                              | [SHA1]()                                                     |

